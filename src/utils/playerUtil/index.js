import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Video from 'react-native-video';
import {Spinner} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

import Constants from './../../shared/constants';
import axios from '../../utils/interceptor';

export default class Player extends Component {
  state = {
    token: null,
  };

  async componentDidMount() {
    try {
      let token = await AsyncStorage.getItem('login_token');
      if (token) {
        this.setState({
          token,
        });
      }
    } catch (error) {
      console.log('error from token fech', error);
    }
  }
  render() {
    return this.state.token === null ? (
      <Spinner color="green" />
    ) : (
      <Video
        source={{
          uri: `${Constants.baseUrl}dashboard/getstreem/${this.props.route.params}`,
          headers: {
            Authorization: `Bearer ${this.state.token}`,
          },
        }}
        ref={(ref) => (this.player = ref)}
        style={styles.backgroundVideo}
        fullscreen={true}
        resizeMode="stretch"
        controls={true}
      />
    );
  }
}

var styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
