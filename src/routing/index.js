import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import DashboardHome from '../screens/dashboard/owndashboard';
import SharedDashboard from '../screens/dashboard/shareddashboard';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          if (route.name === 'OWN') {
            iconName = 'file-tray-full';
          } else if (route.name === 'SHARED') {
            iconName = 'share-social-sharp';
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen name="OWN" component={DashboardHome} />
      <Tab.Screen name="SHARED" component={SharedDashboard} />
    </Tab.Navigator>
  );
}
