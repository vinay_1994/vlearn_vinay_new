import React, {useState} from 'react';
import {StyleSheet, Text, View, Alert} from 'react-native';
import {Item, Input, Label, Button, Spinner} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

import Service from './service';

export default function Home({navigation}) {
  const [contact_num, setname] = useState('');
  const [password, setPass] = useState('');
  const [isLoading, setLoading] = useState(false);

  const handleSubmit = () => {
    setLoading(true);
    Service.login({contact_num, password}, async (err, data) => {
      setLoading(false);
      if (!err) {
        if (data.status === 'OK') {
          try {
            await AsyncStorage.setItem('login_token', data.payLoad.token);
            navigation.navigate('Dashboard');
          } catch (e) {
            console.log(e);
          }
        } else {
          Alert.alert(data.errorMessage);
        }
      }
    });
  };
  return (
    <View style={styles.content}>
      {isLoading ? (
        <Spinner color="green" />
      ) : (
        <View>
          <Item style={styles.fields} floatingLabel>
            <Label>Contact Number</Label>
            <Input onChangeText={(text) => setname(text)} />
          </Item>
          <Item style={styles.fields} floatingLabel>
            <Label>Password</Label>
            <Input onChangeText={(text) => setPass(text)} />
          </Item>
          <Button style={styles.fields} block onPress={handleSubmit}>
            <Text>LOGIN</Text>
          </Button>
          <Button
            style={styles.fields}
            block
            warning
            onPress={() => navigation.navigate('Register')}>
            <Text>REGISTER</Text>
          </Button>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    //alignItems: 'center',
    padding: 40,
  },
  fields: {
    margin: 10,
  },
});
