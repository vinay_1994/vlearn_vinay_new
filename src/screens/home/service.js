import axios from 'axios';
import Constants from './../../shared/constants';

export default class Service {
  static login(data, cb) {
    axios
      .post(Constants.baseUrl + 'auth/login', {...data})
      .then((res) => {
        if (res.data) {
          cb(false, res.data);
        } else {
          cb(true, res.data);
        }
      })
      .catch((rej) => cb(true, rej));
  }
}
