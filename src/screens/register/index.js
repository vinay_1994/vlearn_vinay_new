import React, {Component} from 'react';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Picker,
} from 'native-base';

export default class Register extends Component {
  state = {
    contact_num: null,
    password: '',
    country_code: undefined,
  };

  onInputChange = (a, b) => {
    console.log(a, b);
    this.setState({
      [a]: a === 'country_code' ? b : b.nativeEvent.text,
    });
  };

  render() {
    let {country_code} = this.state;
    return (
      <Container>
        <Content>
          <Form>
            <Item error={true} floatingLabel>
              <Label>Username</Label>
              <Input onChange={(e) => this.onInputChange('contact_num', e)} />
            </Item>
            <Item floatingLabel last>
              <Label>Password</Label>
              <Input
                secureTextEntry
                onChange={(e) => this.onInputChange('password', e)}
              />
            </Item>
            <Item picker>
              <Picker
                mode="dropdown"
                //iosIcon={<Icon name="arrow-down" />}
                style={{width: 50, margin: 50}}
                placeholder="Select your SIM"
                placeholderStyle={{color: '#bfc6ea'}}
                placeholderIconColor="#007aff"
                selectedValue={country_code}
                onValueChange={(e) => this.onInputChange('country_code', e)}>
                <Picker.Item label="INDIA" value="INDIA" />
                <Picker.Item label="US" value="US" />
              </Picker>
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}
