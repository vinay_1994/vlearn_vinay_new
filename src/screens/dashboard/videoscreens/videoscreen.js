import React, {Component} from 'react';
import {
  Container,
  H1,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Spinner,
  Icon,
} from 'native-base';

export default class VideoPage extends Component {
  render() {
    return !this.props.data ? (
      <Spinner color="green" />
    ) : (
      <Container>
        <Content>
          {this.props.data.VIDEO.length === 0 ? (
            <H1
              style={{
                padding: 150,
              }}>
              Empty
            </H1>
          ) : (
            this.props.data.VIDEO.map((i, key) => (
              <List key={key}>
                <ListItem thumbnail>
                  <Left>
                    <Thumbnail
                      square
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTDodDARZXTg_rL44APS1z7mx6jG8--FmNKWA&usqp=CAU',
                      }}
                    />
                  </Left>
                  <Body>
                    <Text>{i.original_filename}</Text>
                    <Text note numberOfLines={1}>
                      Size : {Number(i.file_size) / 1000} Kb
                    </Text>
                  </Body>
                  {this.props.route.name === 'OWN' && (
                    <Icon
                      name="ios-share-social-outline"
                      onPress={() =>
                        this.props.navigation.navigate('Share', i.media_id)
                      }
                    />
                  )}
                  <Right>
                    <Button
                      onPress={() =>
                        this.props.navigation.navigate(
                          'PlayerScreen',
                          i.media_id,
                        )
                      }
                      transparent>
                      <Text>Play</Text>
                    </Button>
                  </Right>
                </ListItem>
              </List>
            ))
          )}
        </Content>
      </Container>
    );
  }
}
