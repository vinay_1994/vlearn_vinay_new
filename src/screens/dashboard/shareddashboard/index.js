import React, {useEffect, useState} from 'react';
import {Container, Tab, Tabs, TabHeading, Icon, Text} from 'native-base';
import {StyleSheet, Alert} from 'react-native';

import Service from './service';
import AudioScreen from '../audioscreen/audioscreen';
import VideoScreen from '../videoscreens/videoscreen';

export default function SharedDashboard(props) {
  let [dashBoardData, setData] = useState(null);
  useEffect(() => {
    Service.getDashboard(null, (err, data) => {
      if (!err) {
        if (data.status === 'OK') {
          setData(data.payLoad);
        }
      } else {
        Alert.alert(data.message);
      }
    });
  }, []);
  return (
    <Container>
      <Tabs>
        <Tab
          heading={
            <TabHeading>
              <Icon name="musical-notes-outline" />
              <Text>AUDIO</Text>
            </TabHeading>
          }>
          <AudioScreen {...props} data={dashBoardData} tabType="audio_tab" />
        </Tab>
        <Tab
          heading={
            <TabHeading>
              <Icon name="ios-videocam-sharp" />
              <Text>VIDEO</Text>
            </TabHeading>
          }>
          <VideoScreen {...props} data={dashBoardData} tabType="video_tab" />
        </Tab>
      </Tabs>
    </Container>
  );
}

const styles = StyleSheet.create({});
