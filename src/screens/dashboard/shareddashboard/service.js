import axios from '../../../utils/interceptor';

export default class Service {
  static getDashboard(data, cb) {
    axios
      .get('dashboard/shr')
      .then((res) => {
        if (res.data) {
          cb(false, res.data);
        } else {
          cb(true, null);
        }
      })
      .catch((rej) => cb(true, null));
  }
}
