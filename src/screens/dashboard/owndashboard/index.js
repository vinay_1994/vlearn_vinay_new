import React, {Component} from 'react';
import {
  Container,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Text,
  Fab,
  Button,
  View,
} from 'native-base';
import {Modal} from 'react-native';

import UploaderModal from './../uploaderscreen/uploader';
import Service from './service';
import AudioScreen from '../audioscreen/audioscreen';
import VideoScreen from '../videoscreens/videoscreen';

export default class OwnDashboard extends Component {
  state = {
    dashBoardData: null,
    isFabOpen: false,
    isModalVisible: false,
    uploadType: null,
  };

  componentDidMount() {
    Service.getDashboard(null, (err, data) => {
      if (!err) {
        if (data.status === 'OK') {
          this.setState({dashBoardData: data.payLoad});
        }
      } else {
        Alert.alert(data.message);
      }
    });
  }

  onFabIcon = (type) => {
    this.setState({
      isModalVisible: true,
      uploadType: type,
    });
  };

  closeUploaderModal = () => {
    this.setState({
      isModalVisible: false,
      uploadType: null,
    });
  };

  render() {
    let {isFabOpen, isModalVisible, type} = this.state;
    return (
      <Container>
        <Tabs>
          <Tab
            heading={
              <TabHeading>
                <Icon name="musical-notes-outline" type="Ionicons" />
                <Text>AUDIO</Text>
              </TabHeading>
            }>
            <AudioScreen
              {...this.props}
              data={this.state.dashBoardData}
              tabType="audio_tab"
            />
          </Tab>
          <Tab
            heading={
              <TabHeading>
                <Icon name="ios-videocam-sharp" type="Ionicons" />
                <Text>VIDEO</Text>
              </TabHeading>
            }>
            <VideoScreen
              {...this.props}
              data={this.state.dashBoardData}
              tabType="video_tab"
            />
          </Tab>
        </Tabs>
        <View style={{flex: 1}}>
          <Fab
            active={this.state.isFabOpen}
            direction="up"
            containerStyle={{}}
            style={{backgroundColor: '#5067FF'}}
            position="bottomRight"
            onPress={() => this.setState({isFabOpen: !isFabOpen})}>
            <Icon name="add-circle-outline" type="Ionicons" />
            <Button
              onPress={() => this.onFabIcon('audio')}
              style={{backgroundColor: '#34A34F'}}>
              <Icon name="musical-notes-outline" />
            </Button>
            <Button
              onPress={() => this.onFabIcon('video')}
              style={{backgroundColor: '#3B5998'}}>
              <Icon name="ios-videocam-sharp" type="Ionicons" />
            </Button>
          </Fab>
        </View>
        <Modal visible={isModalVisible} animationType="fade">
          <UploaderModal
            type={type}
            closeUploaderModal={this.closeUploaderModal}
          />
        </Modal>
      </Container>
    );
  }
}

//const styles = StyleSheet.create({});
