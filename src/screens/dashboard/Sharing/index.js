import React, {Component} from 'react';
import {Alert, StyleSheet} from 'react-native';
import {
  Container,
  Content,
  List,
  ListItem,
  Text,
  Body,
  Right,
  Spinner,
  Button,
} from 'native-base';
import axios from '../../../utils/interceptor';

export default class Sharing extends Component {
  state = {
    userList: null,
  };

  componentDidMount() {
    axios
      .get('dashboard/shr/getuserslist')
      .then((res) => {
        if (res.data.status === 'OK') {
          this.setState({
            userList: res.data.payLoad,
          });
        } else {
          Alert.alert('Something went wrog');
          this.props.navigation.goBack();
        }
      })
      .catch((rej) => {
        Alert.alert(rej.toString());
        this.props.navigation.goBack();
      });
  }

  onSharePress = (user) => {
    this.setState({
      isButtonDisabeld: true,
    });
    axios
      .post(`dashboard/shr/shareto/${this.props.route.params}`, {
        ...user,
      })
      .then((res) => {
        if (res.data.status === 'OK') {
          this.setState(
            {
              isButtonDisabeld: false,
            },
            () => {
              Alert.alert(res.data.payLoad);
              this.props.navigation.goBack();
            },
          );
        } else {
          Alert.alert(res.data.errorMessage);
          this.setState({
            isButtonDisabeld: false,
          });
        }
      })
      .then((rej) => {
        this.setState({
          isButtonDisabeld: false,
        });
      });
  };

  render() {
    let {userList, isButtonDisabeld} = this.state;
    return (
      <Container>
        <Content>
          <List>
            {userList ? (
              userList.map((i, key) => (
                <ListItem thumbnail key={key}>
                  <Body>
                    <Text>{i.contact_num}</Text>
                    <Text note numberOfLines={1}>
                      click share button to share to above number . .
                    </Text>
                  </Body>
                  <Right>
                    <Button
                      disabled={isButtonDisabeld}
                      transparent
                      onPress={() => this.onSharePress(i)}>
                      <Text>SHARE</Text>
                    </Button>
                  </Right>
                </ListItem>
              ))
            ) : (
              <Spinner color="blue" />
            )}
          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
