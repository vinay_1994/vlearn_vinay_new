import React from 'react';
import {Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import DashboardRotes from './src/routing';
import LandingPage from './src/screens/home';
import PlayerUtil from './src/utils/playerUtil';
import Sharing from './src/screens/dashboard/Sharing';
import Register from './src/screens/register';

const StackNavigator = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <StackNavigator.Navigator initialRouteName="Home">
        <StackNavigator.Screen name="Home" component={LandingPage} />
        <StackNavigator.Screen name="Dashboard" component={DashboardRotes} />
        <StackNavigator.Screen name="Register" component={Register} />
        <StackNavigator.Screen name="PlayerScreen" component={PlayerUtil} />
        <StackNavigator.Screen name="Share" component={Sharing} />
      </StackNavigator.Navigator>
    </NavigationContainer>
  );
}
